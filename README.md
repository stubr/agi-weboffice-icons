# README #

WebOffice-Icons und Stylesheet

### Welche Dateien benötige ich? ###

Alle notwendigen Resourcen finden sich unter diesem Verzeichnis:
 

```
#!html

/dist/webserver/
```
die beiden Verzeichnisse **client_core** und **pub** können direkt auf dem WebOffice-Server mit dem Inhalt dieser Verzeichnisse überschrieben werden.

Um das Stylesheet anzupassen, ist nur diese Datei notwendig:


```
#!html

pub/client_core/styles/weboffice_user.css
```

### Wer ist verantwortlich für dieses Repo? ###

* Bernhard Sturm ([bs@sturmundbraem.ch](mailto:bs@sturmundbraem.ch))